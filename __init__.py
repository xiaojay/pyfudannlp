#coding=utf-8
import os, subprocess, urllib
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
HOST = 'jkx.fudan.edu.cn/fudannlp/'

def seg(sentence):
    c = 'cd %s;java -Xmn16m -Xms64m -Xmx256m -classpath fudannlp.jar:lib/commons-cli-1.2.jar:lib/trove.jar edu.fudan.nlp.cn.tag.CWSTagger -s models/seg.m "%s"'
    p = subprocess.Popen(c%(PROJECT_ROOT,sentence), shell=True, stdout=subprocess.PIPE)
    r = p.communicate()
    return unicode(r[0], 'gbk').split()

def pos(sentence):
    c = 'cd %s;java -Xmn16m -Xms64m -Xmx256m -classpath fudannlp.jar:lib/commons-cli-1.2.jar:lib/trove.jar edu.fudan.nlp.cn.tag.POSTagger -s models/seg.m models/pos.m "%s"'
    p = subprocess.Popen(c%(PROJECT_ROOT,sentence), shell=True, stdout=subprocess.PIPE)
    r = p.communicate()
    return unicode(r[0], 'gbk').split()

def ner(sentence):
    c = 'cd %s;java -Xmn16m -Xms64m -Xmx256m -classpath fudannlp.jar:lib/commons-cli-1.2.jar:lib/trove.jar edu.fudan.nlp.cn.tag.NERTagger -s models/seg.m  models/pos.m "%s"'
    p = subprocess.Popen(c%(PROJECT_ROOT,sentence), shell=True, stdout=subprocess.PIPE)
    r = p.communicate()
    return unicode(r[0], 'gbk')

#web service
def _request(sentence, service='seg'):
    '''sentene utf-8 encoded or unicode'''
    if type(sentence) == type(u'u'):
        sentence = sentence.encode('u8')
    url = 'http://'+urllib.quote(HOST+'%s/'%service+sentence)
    resp = urllib.urlopen(url).read()
    resp = resp.split('\n')[1]
    return resp

def seg2(sentence):
    r = _request(sentence, 'seg')
    r = [unicode(i, 'u8') for i in r.split()]
    return r

def pos2(sentence):
    r = _request(sentence, 'pos')
    r = [[unicode(i.split('/')[0], 'u8'), unicode(i.split('/')[1], 'u8')] for i in r.split()]
    return r

if __name__ == '__main__':
    import sys
    r = pos(sys.argv[1])
    for i in r: print i
